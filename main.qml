import QtQml 2.0
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import BackEnd 1.0

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    BackEnd{
        objectName: "back"
        id: pom
        textik: "Tu sa vam zobrazi vysledok"
    }

   GridLayout{
       columns: 1
       anchors.centerIn: parent

       Rectangle{
           id : "skore"
           Layout.preferredHeight: 100
           Layout.preferredWidth: 200

           Text {
               id: "skore2"
               text: pom.skore
               font.pointSize: 20
               font.bold: true
               color: "green"
               anchors.centerIn: parent
           }
       }


       ToolBar{

           RowLayout{
                id : searchBar
                visible: true

                TextField{
                    id: searchText
                    visible: true
                    placeholderText: "Zadajte cislo"
                    Layout.fillWidth: true
                }

                ToolButton{
                    signal posli(int cislo)
                    id : searchButton
                    objectName: "cisloButton"
                    text: "Hadaj"
                    width: parent.width
                    onClicked: {
                        searchButton.posli(parseInt(searchText.text))
                    }
                }
           }
       }

       Rectangle{
           id : zobrazenie
           Layout.preferredHeight: 100
           Layout.preferredWidth: 200

           Text {
               id: zobrazenieText
               text: pom.textik
               font.pointSize: 12
               font.bold: true
               anchors.centerIn: parent
           }
       }
   }
}

