#include "hodnoty.h"

Hodnoty::Hodnoty()
{
    _cislo = 50;
}

void Hodnoty::setTextik(const QString &novy)
{
    if(novy == _text)
        return;

    _text = novy;
    emit vlastnyChangeText();
}

QString Hodnoty::getTextik() const
{
    return _text;
}

void Hodnoty::skontroluj(const qint32& poslane)
{
   if(poslane > _cislo)
   {
       _text = "menej";
   }
   else if(poslane < _cislo)
   {
       _text = "viac";
   }
   else
   {
       _text = "trafil si";
       _cislo = rand() % 100;
       _skore++;
   }

    emit vlastnyChangeText();
   emit upravaSkore();
   // treba davat emit ak je to definovane ako property, ocividne NOTIFY a emit treba vsade ak to nie je constant, aj ked v dokumentacii "OPTIONAL" ???
}

qint64 Hodnoty::getCislo() const
{
    return _cislo;
}

qint64 Hodnoty::getSkore() const
{
    return _skore;
}
