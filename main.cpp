#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "hodnoty.h"
#include <QQmlComponent>
#include <QDebug>

#include <iostream>


int main(int argc, char *argv[])
{
    // registracia vlastnej QObject clasy do QML
    qmlRegisterType<Hodnoty>("BackEnd",1,0,"BackEnd");

    srand(time(NULL));

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    // najdi window
    QList<QObject*> pom = engine.rootObjects();

    // hladanie potrebnych objektov vo window
    QObject* backend = pom[0]->findChild<QObject*>("back",Qt::FindChildrenRecursively);
    QObject* buttonCislo = pom[0]->findChild<QObject*>("cisloButton",Qt::FindChildrenRecursively);

    QObject::connect(buttonCislo,SIGNAL(posli(int)),backend,SLOT(skontroluj(qint32)));

    return app.exec();
}
