#ifndef HODNOTY_H
#define HODNOTY_H

#include <QObject>
#include <QString>
#include <qqml.h>
#include <QMessageLogContext>


class Hodnoty : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString textik READ getTextik WRITE setTextik NOTIFY vlastnyChangeText)
    Q_PROPERTY(qint64 ciselko READ getCislo CONSTANT)
    Q_PROPERTY(qint64 skore READ getSkore NOTIFY upravaSkore)

public:
    Hodnoty();
    QString getTextik() const ;
    qint64 getCislo() const;
    qint64 getSkore() const;

public slots:
    void setTextik(const QString& novy);
    void skontroluj(const qint32& poslane);

signals:
    void vlastnyChangeText(); // signaly sa nedefinuju ??? ocividne nie
    void zmenaCisla(const qint32& poslane);
    void upravaSkore();

private:
    QString _text =  "Ahoj";
    qint64 _cislo = 0;
    qint32 _skore = 0;
};

#endif // HODNOTY_H
